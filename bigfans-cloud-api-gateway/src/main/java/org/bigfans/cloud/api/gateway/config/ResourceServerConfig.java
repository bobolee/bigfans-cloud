package org.bigfans.cloud.api.gateway.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

//@EnableResourceServer
//@Configuration
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

	@Override
	public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
		super.configure(resources);
	}
	
	@Override
	public void configure(HttpSecurity http) throws Exception {
		http
        .authorizeRequests()
        .antMatchers("/user-service/**").hasAuthority("AUTHORITY_SUPER")
        .antMatchers("/addresse-service/**").hasAuthority("AUTHORITY_SUPER")
        .antMatchers("/point-service/**").hasAuthority("AUTHORITY_SUPER")
        .antMatchers("/itemservice-service/**").hasAuthority("AUTHORITY_SUPER")
        .antMatchers("/order-service/**").hasAuthority("AUTHORITY_SUPER")
        .antMatchers("/payment-service/**").hasAuthority("AUTHORITY_SUPER")
        .antMatchers("/deliveriy-service/**").hasAuthority("AUTHORITY_SUPER")
        .anyRequest().authenticated();
	}
	
}
