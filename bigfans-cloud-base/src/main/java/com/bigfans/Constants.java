package com.bigfans;

import java.math.BigDecimal;

/**
 * 
 * @Description:实体类的常量定义
 * @author lichong
 * 2014年12月14日下午4:52:07
 *
 */
public interface Constants {
	
	String SPACE = " ";
	BigDecimal ZERO = new BigDecimal(0);
	BigDecimal ONE_HUNDRED = new BigDecimal(100);
	
	String REQ_PAGESIZE = "ps";
	String REQ_CUR_PAGE = "cp";

	interface TOKEN {
		String KEY_NAME = "user_token";
		String HEADER_KEY_NAME = "Cookie";
	}
	
}
