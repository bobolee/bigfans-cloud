package com.bigfans.catalogservice.api;

import com.bigfans.catalogservice.model.Category;
import com.bigfans.catalogservice.service.category.CategoryService;
import com.bigfans.framework.annotations.NeedLogin;
import com.bigfans.framework.web.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CategoryManageApi {

	@Autowired
	private CategoryService categoryService;
	
	@PostMapping(value = "/category")
	@NeedLogin
	public RestResponse create(@RequestBody Category category) throws Exception{
		categoryService.create(category);
		return RestResponse.ok();
	}
}
