package com.bigfans.catalogservice.api;

import com.bigfans.catalogservice.model.Stock;
import com.bigfans.catalogservice.service.sku.StockService;
import com.bigfans.framework.web.BaseController;
import com.bigfans.framework.web.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lichong
 * @create 2018-04-11 下午7:48
 **/
@RestController
public class StockApi  extends BaseController{

    @Autowired
    private StockService stockService;

    @GetMapping(value = "stock")
    public RestResponse getStock(@RequestParam(value = "prodId") String prodId) throws Exception{
        Stock stock = stockService.getByProd(prodId);
        return RestResponse.ok(stock.getRest());
    }

}
