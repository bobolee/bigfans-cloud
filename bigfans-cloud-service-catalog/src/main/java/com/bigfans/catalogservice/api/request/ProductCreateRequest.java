package com.bigfans.catalogservice.api.request;

import com.bigfans.catalogservice.model.Product;
import com.bigfans.catalogservice.model.ProductGroup;
import com.bigfans.catalogservice.model.ProductGroupAttribute;
import com.bigfans.catalogservice.model.ProductGroupTag;
import lombok.Data;

import java.util.List;

@Data
public class ProductCreateRequest {

	private ProductGroup productGroup;
	private List<Product> products;
	private List<ProductGroupAttribute> attributes;
	private List<ProductGroupTag> tags;

}
