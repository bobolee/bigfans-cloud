package com.bigfans.catalogservice.listener;

import com.bigfans.model.event.CategoryCreatedEvent;
import com.bigfans.framework.event.EventRepository;
import com.bigfans.framework.kafka.KafkaTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;


/**
 * @author lichong
 * @create 2018-02-05 下午8:08
 **/
@Component
public class CategoryEventListener {

    @Autowired
    private KafkaTemplate kafkaTemplate;
    @Autowired
    private EventRepository eventRepository;

    private static final Logger logger = LoggerFactory.getLogger(CategoryEventListener.class);

    @Transactional
    @TransactionalEventListener(phase = TransactionPhase.BEFORE_COMMIT)
    public void on(CategoryCreatedEvent event) {
        eventRepository.save(event);
        kafkaTemplate.send(event);
        logger.debug("CategoryCreatedEvent invoked");
    }

}
