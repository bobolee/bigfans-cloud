package com.bigfans.catalogservice.model;

import lombok.Data;

import java.io.Serializable;


/**
 * 
 * @Description:图片组
 * @author lichong 2015年3月19日上午9:16:58
 *
 */
@Data
public class ImageGroup implements Serializable{

	private static final long serialVersionUID = 6897123351221622280L;
	private String pid;
	private String smallImg;
	private String middleImg;
	private String largeImg;
	private Integer orderNum;
	private String server;

	@Override
	public String toString() {
		return "ImageGroup [smallImg=" + smallImg + ", middleImg="
				+ middleImg + ", largeImg=" + largeImg + "]";
	}

}
