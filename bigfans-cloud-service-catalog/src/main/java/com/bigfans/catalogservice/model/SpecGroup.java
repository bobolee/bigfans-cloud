package com.bigfans.catalogservice.model;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 商品规格组,封装商品规格项->规格值
 * 
 * @author lichong
 *
 */
@Data
public class SpecGroup implements Serializable{

	private static final long serialVersionUID = 7399149219218538867L;
	
	private SpecOption option;
	private List<ProductSpec> values;

}
