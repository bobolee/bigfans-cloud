package com.bigfans.catalogservice.model.entity;

import com.bigfans.framework.model.AbstractModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;

@Data
@Table(name="ProductGroup_Attribute")
public class ProductGroupAttributeEntity extends AbstractModel {

	private static final long serialVersionUID = -6811834593128832769L;

	@Column(name="pg_id")
	protected String pgId;
	@Column(name="option_id")
	protected String optionId;
	@Column(name="value_id")
	protected String valueId;
	@Column(name="order_num")
	protected Integer orderNum;

	@Override
	public String getModule() {
		return "ProductGroupAttribute";
	}

}
