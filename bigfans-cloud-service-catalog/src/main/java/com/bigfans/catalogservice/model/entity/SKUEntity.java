package com.bigfans.catalogservice.model.entity;

import com.bigfans.framework.model.AbstractModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;

/**
 * 
 * @Description:SKU
 * @author lichong 2015年8月9日下午7:51:58
 *
 */
@Data
@Table(name="SKU")
public class SKUEntity extends AbstractModel {

	public String getModule() {
		return "SKU";
	}

	private static final long serialVersionUID = -4643063498862312784L;

	@Column(name = "prod_id")
	protected String prodId;
	@Column(name = "pg_id")
	protected String pgId;
	@Column(name="sku_key")
	protected String skuKey;
	@Column(name="opt_key")
	protected String optKey;
	@Column(name="val_key")
	protected String valKey;
	@Column(name="stock")
	protected Integer stock;
	@Column(name="locked_stock")
	protected Integer lockedStock;

}
