package com.bigfans.orderservice.service.impl;

import com.bigfans.framework.dao.BaseServiceImpl;
import com.bigfans.orderservice.dao.InvoiceDAO;
import com.bigfans.orderservice.model.Invoice;
import com.bigfans.orderservice.service.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 
 * @Description:发票服务类
 * @author lichong
 * 2015年7月10日上午9:14:53
 *
 */
@Service(InvoiceServiceImpl.BEAN_NAME)
public class InvoiceServiceImpl extends BaseServiceImpl<Invoice> implements InvoiceService {

	public static final String BEAN_NAME = "invoiceService";
	
	private InvoiceDAO invoiceDAO;
	
	@Autowired
	public InvoiceServiceImpl(InvoiceDAO invoiceDAO) {
		super(invoiceDAO);
		this.invoiceDAO = invoiceDAO;
	}
}
