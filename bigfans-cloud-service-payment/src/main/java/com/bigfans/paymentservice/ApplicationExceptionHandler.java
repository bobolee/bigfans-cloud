package com.bigfans.paymentservice;

import com.bigfans.framework.exception.GlobalExceptionHandler;
import org.springframework.web.bind.annotation.ControllerAdvice;

/**
 * @author lichong
 * @create 2018-04-09 下午9:48
 **/
@ControllerAdvice
public class ApplicationExceptionHandler extends GlobalExceptionHandler {
}
