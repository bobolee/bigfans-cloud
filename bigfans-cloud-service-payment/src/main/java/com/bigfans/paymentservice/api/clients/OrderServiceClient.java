package com.bigfans.paymentservice.api.clients;

import com.bigfans.framework.utils.BeanUtils;
import com.bigfans.framework.web.RestResponse;
import com.bigfans.paymentservice.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * @author lichong
 * @create 2018-02-25 下午4:21
 **/
@Component
public class OrderServiceClient {

    @Autowired
    private RestTemplate restTemplate;

    public CompletableFuture<Order> getOrder(String orderId){
        return CompletableFuture.supplyAsync(() -> {
            UriComponents builder = UriComponentsBuilder.fromUriString("http://order-service/orders/{id}").build().expand(orderId).encode();
            ResponseEntity<RestResponse> responseEntity = restTemplate.getForEntity(builder.toUri(), RestResponse.class);
            RestResponse restResponse = responseEntity.getBody();
            Map data = (Map) restResponse.getData();
            Order order = new Order();
            BeanUtils.mapToModel(data , order);
            return order;
        });
    }
}
