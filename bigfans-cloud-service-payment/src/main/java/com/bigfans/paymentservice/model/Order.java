package com.bigfans.paymentservice.model;

import com.bigfans.paymentservice.model.entity.OrderEntity;
import lombok.Data;

/**
 * 
 * @Description:订单
 * @author lichong 
 * 2014年12月14日下午4:07:01
 *
 */
@Data
public class Order extends OrderEntity {

	private static final long serialVersionUID = -1502505888265346945L;

}
