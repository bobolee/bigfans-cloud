package com.bigfans.searchservice.model.vo;

import lombok.Data;

/**
 * @author lichong
 * @create 2018-03-18 下午2:20
 **/
@Data
public class SearchFilterItem {
    private String key;
    private String label;
    private String url;

    public SearchFilterItem(String key , String label , String url) {
        this.key = key;
        this.label = label;
        this.url = url;
    }
}
