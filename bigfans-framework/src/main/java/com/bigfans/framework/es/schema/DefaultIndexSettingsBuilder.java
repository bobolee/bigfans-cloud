package com.bigfans.framework.es.schema;

public class DefaultIndexSettingsBuilder extends IndexSettingsBuilder{
	
	public DefaultIndexSettingsBuilder() {
		this.setNumberOfShards(3);
		this.setNumberOfReplicas(1);
//		this.setCompress(true);
//		this.setCompressStored(true);
//		this.setCompressTV(true);
		this.setRefreshInterval("5s");
		this.setTranslogFlushThresholdSize("5gb");
		
		this.putSettings(new IndexSettingsFilter_NGram_2_10());
		this.putSettings(new IndexSettingsAnalyzer_IK());
		this.putSettings(new IndexSettingsAnalyzer_Pinyin());
	}
	
}
